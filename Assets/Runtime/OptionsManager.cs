﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OptionsManager : Singleton<OptionsManager>
{
    public const string PRIMARY_HIGHLIGHT_OPENING_TAGS = "{[PRIMARY_HIGHLIGHT_OPENING_TAGS]}";
    public const string PRIMARY_HIGHLIGHT_CLOSING_TAGS = "{[PRIMARY_HIGHLIGHT_CLOSING_TAGS]}";
    public const string SECONDARY_HIGHLIGHT_OPENING_TAGS = "{[SECONDARY_HIGHLIGHT_OPENING_TAGS]}";
    public const string SECONDARY_HIGHLIGHT_CLOSING_TAGS = "{[SECONDARY_HIGHLIGHT_CLOSING_TAGS]}";

    [Header("Visual Configuration")]
    [SerializeField] private Color _primaryHighlightColor = new Color(0, 0, 0, 1);
    [SerializeField] private Color _secondaryHighlightColor = new Color(0, 0, 0, 1);

    [SerializeField] private Dictionary<string, object> _globalLocalizationParameters;

    [Header("Assistive Configuration")]
    [SerializeField] private Color _defaultTimerColor = new Color(0, 0, 0, 1);
    [SerializeField] private Color _inactiveTimerColor = new Color(0, 0, 0, 1);
    [Space]
    [SerializeField] private Color _manualTimerColor = new Color(0, 0, 0, 1);
    [SerializeField] private float _manualOverrideTimeout = 3f;
    [SerializeField] private float _manualReturnOffset = 1f;
    [SerializeField, ReadOnly] private bool _isManualOverrideActive;


    [Header("Animation Configuration")]
    [SerializeField] private float _transitionDuration = 1f;
    [SerializeField] private float _carouselSpeed = 2f;


#if UNITY_EDITOR
    [Header("Developer Options")]
    [SerializeField] private bool _enableDeveloperOverrides;
    [SerializeField] private string _devLanguageCode = "de";
    [SerializeField] private float _devWelcomeTimerDuration = 1f;
    [SerializeField] private float _devAnimationDuration = .25f;
    [SerializeField] private float _devTextDisplayDuration = 1f;

    public static bool EnableDeveloperOverrides { get => Instance._enableDeveloperOverrides; set => Instance._enableDeveloperOverrides = value; }
    public static float DevAnimationDuration { get => Instance._devAnimationDuration; set => Instance._devAnimationDuration = value; }
    public static float DevWelcomeTimerDuration { get => Instance._devWelcomeTimerDuration; set => Instance._devWelcomeTimerDuration = value; }
    public static float DevTextDisplayDuration { get => Instance._devTextDisplayDuration; set => Instance._devTextDisplayDuration = value; }
    public static string DevLanguageCode { get => Instance._devLanguageCode; set => Instance._devLanguageCode = value; }

#endif

    public static float TransitionDuration { get => Instance._transitionDuration; set => Instance._transitionDuration = value; }
    public static float ManualOverrideTimeout { get => Instance._manualOverrideTimeout; set => Instance._manualOverrideTimeout = value; }
    public static float ManualReturnTimeOffset { get => Instance._manualReturnOffset; set => Instance._manualReturnOffset = value; }
    public static Color ActiveTimerColor => IsManualOverrideActive ? ManualTimerColor : DefaultTimerColor;
    public static Color DefaultTimerColor { get => Instance._defaultTimerColor; set => Instance._defaultTimerColor = value; }
    public static Color InactiveTimerColor { get => Instance._inactiveTimerColor; set => Instance._inactiveTimerColor = value; }
    public static bool IsManualOverrideActive { get => Instance._isManualOverrideActive; set => Instance._isManualOverrideActive = value; }
    public static Color ManualTimerColor { get => Instance._manualTimerColor; set => Instance._manualTimerColor = value; }
    public static float CarouselSpeed
    {
        get
        {
            return Mathf.Min(Instance._carouselSpeed * TransitionDuration, WebGLPreferences.Current.ReactionTime) / 2f;
        }
        set => Instance._carouselSpeed = value;
    }

    public Color PrimaryHighlightColor { get => _primaryHighlightColor; set => _primaryHighlightColor = value; }
    public string GetPrimaryHighlightOpeningTags => "<#" + ColorUtility.ToHtmlStringRGBA(PrimaryHighlightColor) + ">";
    public string GetPrimaryHighlightClosingTags => "</color>";

    public Color SecondaryHighlightColor { get => _secondaryHighlightColor; set => _secondaryHighlightColor = value; }
    public string GetSecondaryHighlightOpeningTags => "<#" + ColorUtility.ToHtmlStringRGBA(SecondaryHighlightColor) + ">"; 
    public string GetSecondaryHighlightClosingTags => "</color>";

    public static Dictionary<string, object> GlobalLocalizationParameters { get => Instance._globalLocalizationParameters; set => Instance._globalLocalizationParameters = value; }

    public static string ParseLocalizationParams(string text)
    {
        RefreshParameterDictionary();

        var returnString = text;
        //Debug.Log(string.Join("; ", GlobalLocalizationParameters.Where(x => returnString.Contains(x.Key)).Select(x => string.Format("{0}{1}{2}", x.Key, "->", x.Value))));

        while (GlobalLocalizationParameters.Keys.Any(returnString.Contains))
        {
            var param = GlobalLocalizationParameters.First(x => returnString.Contains(x.Key));
            returnString = returnString.Replace(param.Key.ToString(), param.Value.ToString());
        }

        return returnString;
    }

    private static void RefreshParameterDictionary()
    {
        GlobalLocalizationParameters = new Dictionary<string, object>()
        {
            { PRIMARY_HIGHLIGHT_OPENING_TAGS , Instance.GetPrimaryHighlightOpeningTags },
            { PRIMARY_HIGHLIGHT_CLOSING_TAGS , Instance.GetPrimaryHighlightClosingTags },
            { SECONDARY_HIGHLIGHT_OPENING_TAGS , Instance.GetSecondaryHighlightOpeningTags },
            { SECONDARY_HIGHLIGHT_CLOSING_TAGS , Instance.GetSecondaryHighlightClosingTags },
        };
    }

    /*
    // Register this class every time this object is enabled
    public void OnEnable()
    {
    if (!LocalizationManager.ParamManagers.Contains(this))
    {
    LocalizationManager.ParamManagers.Add(this);
    LocalizationManager.LocalizeAll(true);
    }
    }


    // If the object is disabled, then unregister it
    public void OnDisable()
    {
    LocalizationManager.ParamManagers.Remove(this);
    }

    public string GetParameterValue(string paramName)
    {
    Debug.Log("paramName");
    return _globalLocalizationParameters.ContainsKey("paramName")
    ? _globalLocalizationParameters[paramName]
    : null;
    }
    */
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(Instance);
#if UNITY_EDITOR
        if (EnableDeveloperOverrides)
        {
            TransitionDuration = DevAnimationDuration;
        }
#endif
    }
}
